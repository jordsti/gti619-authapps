﻿using GTI619_LAB5.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;

namespace GTI619_LAB5.Core
{
    public class Authorization : FilterAttribute, IAuthorizationFilter
    {
        private readonly UserRoles[] _acceptedRoles;

        public Authorization(params UserRoles[] acceptedroles)
        {
            _acceptedRoles = acceptedroles;
        }

        public Authorization(params bool[] allowAll)
        {
            if (allowAll[0])
                _acceptedRoles = new[] { UserRoles.Admin, UserRoles.None, UserRoles.Circle, UserRoles.Square };
        }

        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if (SessionHelper.UserInSession == null)//user not logged in
            {
                string retUrl = filterContext.HttpContext.Request.RawUrl;

                FormsAuthentication.SignOut();
                
                filterContext.Result =
                     new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary {{ "controller", "home" },
                                             { "action", "index" },
                                             { "returnUrl",    retUrl } }); //send the user to login page with return url
                return;
            }
            if (!_acceptedRoles.Any(acceptedRole => SessionHelper.UserInSession.Roles.Any(currentRole => acceptedRole == currentRole)))
            {
                filterContext.Result = new ViewResult
                {
                    ViewName = "~/Views/Shared/Error.cshtml"
                };
            }
        }
    }

}