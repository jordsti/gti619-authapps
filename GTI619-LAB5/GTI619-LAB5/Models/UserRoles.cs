﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GTI619_LAB5.Models
{
    public static enum UserRoles
    {
        None = 0,

        Admin = 1,
            
        Square = 2,
            
        Circle = 3
    }
}